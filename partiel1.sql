set serveroutput on;
-- Les contraintes

drop table client cascade constraints;
drop table produit cascade constraints;
drop table commande cascade constraints;
drop table retour cascade constraints;

-- 1 contrainte not null

-- 7 contraintes 
create table client (
    idcl int primary key, -- id de client 
    nomcl varchar2(20) not null, -- nom de client
    unique (nomcl),
    telephone int not null,
    unique (telephone),
    check (telephone >= 0),
    check (telephone <= 9999999999),
    avoir int default 0 not null,
    check (avoir >= 0),
    check (avoir <= 10000)
);

-- 5 contraintes
create table produit (
    idp int primary key, -- id produit
    nompr varchar2(20) not null, -- nom produit
    prix int not null,
    check(prix > 0),
    check(prix <= 10000),
    stock int not null,
    check(stock >= 0),
    check(stock <= 10000)
);

-- 8 contraintes
create table commande (
    idco int primary key, -- id de commande
    idcl int not null, -- id de client
    foreign key ( idcl ) references client,
    idp int not null, -- id de produit
    foreign key ( idp ) references produit,
    quantite int not null,
    check(quantite > 0),
    check(quantite <= 10000),
    jour int not null,
    check(jour >= 1),
    check(jour <= 365),
    unique(idcl,jour)
);

-- 6 contraintes
create table retour (
    idr int primary key, -- id de retour
    idco int not null, -- id de commande
    unique(idco),
    foreign key ( idco ) references commande,
    jour int not null,
    check(jour >= 1),
    check(jour <= 365),
    statut varchar2(20) not null,
    check(statut in ('a_traiter','traite'))
);

-- Contraintes non SQL :

-- 1. Une seule action par jour pour l'utilisateur
-- Pour tout idc, soit J l'ensemble des jours associés aux commandes ET retours faites par idc. Tout jour
-- dans J sont différent

-- 2. Un client ne peut commander un produit si il y a un retour non traité correspondant à ce produit
-- Pour tout retour r, soit idc le client, co la commande et j le jour associé à r.
-- Soit pr le produit associé à co
-- Si le retour est pas traité, alors il n'existe pas de commandé co' tel que sont jour est > j, 
-- le produit = pr et le client = idc

-- 3. Cohérence entre avoir, quantité et prix 
-- Soit idc un client. On note P la somme des (prix * quantite) de chaque produit associé a chaque commande associé a idc.
-- On note P' la somme des (prix * quantite) de chaque produit associé a chaque commande associé à chaque retour associé à idc.
-- avoir - P + P' = 0 ( en suppossant qu'on ne peut pas rajouter de l'argent )

-- 4. Retour apres une commande
-- Pour chaque retour, soit c la commande associé. jour de c < jour de r

-- 5. Stock et quantite dans les commandes
-- Pour chaque produit p. 
-- On note C l'ensemble des commandes associé à p pour lequel il n'existe pas de retour R associé.
-- On note Q la somme des quantités des commandes dans C.
-- Q <= stock du produit p

-- Insertions
insert into client
values (1,'Riton',0698765432,50);

insert into produit
values (10,'cravate',15,157);

insert into commande
values (20,1,10,2,32);

insert into retour
values (30,20,33,'a_traiter');

-- PL/SQL
create or replace procedure traitement4 (
  l_avoir_total out client.avoir%type,
  l_qte_remise out commande.quantite%type
)
is
    cursor c is
        select idr, commande.idp, commande.idcl, produit.prix, commande.quantite, client.avoir
            from retour,commande,produit,client
            where retour.idco = commande.idco 
            and commande.idp = produit.idp
            and commande.idcl = client.idcl
            order by retour.jour asc;
    l_idr retour.idr%type;
    l_idp commande.idp%type;
    l_idcl commande.idcl%type;
    le_prix produit.prix%type;
    l_avoir client.avoir%type;
begin
    open c;
    fetch c into l_idr, l_idp, l_idcl, le_prix, l_qte_remise, l_avoir;
    if c%notfound then
        l_avoir_total := -1;
        l_qte_remise := -1;
    else
        -- on met a jour le retour en traite
        update retour
            set statut = 'traite'
            where idr = l_idr;
        -- calcul le nouveau avoir
        l_avoir_total := l_avoir + (le_prix * l_qte_remise);
        -- on met a jour l'avoir du client
        update client
            set avoir = l_avoir_total
            where idcl = l_idcl;
        -- on met a jour le stock
        update produit
            set stock = stock + l_qte_remise
            where idp = l_idp;
    end if;
    close c;
end;
/

declare
    l_avoir_total client.avoir%type;
    l_qte_remise commande.quantite%type;
begin
    traitement4(l_avoir_total,l_qte_remise);
    dbms_output.put_line( l_avoir_total || ' ' || l_qte_remise);
end;
/