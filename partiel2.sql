set serveroutput on;

drop table client cascade constraints;
drop table typevoiture cascade constraints;
drop table offre cascade constraints;
drop table demande cascade constraints;
drop table covoiturage cascade constraints;

-- 1 contrainte not null

-- 4 contraintes
create table client (
    idc int primary key,
    nom varchar(20) not null,
    telephone int,
    unique(telephone),
    check(telephone >= 0),
    check(telephone <= 9999999999)
);

-- 2 contraintes
create table typevoiture (
    idt int primary key,
    marque varchar(20) not null,
    nb_siege int not null,
    unique(marque,nb_siege)
);

-- 4 contraintes
create table offre (
    ido int primary key,
    idc int not null,
    foreign key (idc) references client,
    unique(idc),
    idt int not null,
    foreign key (idt) references typevoiture,
    destination varchar2(20) not null
);

-- 4 contraintes
create table demande (
    idd int primary key,
    idc int not null,
    foreign key (idc) references client,
    destination varchar2(20) not null,
    marque varchar2(20) not null,
    status varchar2(20) not null,
    check(status = 'pieton' or status = 'passager' or status = 'ignore'),
    unique(idc,destination,marque)
);

-- 4 contraintes
create table covoiturage (
    idco int primary key,
    ido int not null,
    foreign key (ido) references offre,
    idc int not null,
    foreign key (idc) references client,
    unique(ido,idc)
);

-- Non SQL :

-- 1. Pour tout IDC dans client, il doit apparaitre dans 
-- demande ou offre

-- 2. Pour chaque IDC dans demande, il ne doivent pas figurer dans offre

-- 3. Et inversement

-- 4. Pour chaque covoiturage. Soit c le client passager. Soit o l'offre associée. Soit d la destination et m la marque associée à o.
-- Il existe un demande tel que la demande est en status passager, la marque est pareil, destination et client aussi

-- 5. Pour chaque idc dans demande, il y en a au plus 1 qui est passager. Si il y en a 1 passager, alors tous les autres sont ignorés.
-- Sinon ils sont tous pietons

-- 6. Pour chaque offre o qui sont associé à au moins 1 covoiturage. Soit t le type de voiture et nb place le nombre de place. Il y a au plus
-- nb covoiturage associé a cette offre.

-- 7. IL y a plus de client que de covoiturage

insert into client
values (1,'Riton','0612345678');

insert into client
values (4,'Rita','1234567890');

insert into client
values (3,'Riti','0000000000');

insert into demande 
values (30,3,'Berlin','Porsche','pieton');

insert into typevoiture
values (10,'Ferrari',2);

insert into offre
values (20,1,10,'SaintTrop');

insert into covoiturage
values (100,20,4);

create or replace procedure traitement2 (
    l_idco out covoiturage.idco%type,
    l_ido out offre.ido%type,
    le_passager out client.idc%type
)
is
    cursor c1 is
        select idd,idc,destination,marque
        from demande
    ;
    l_idd demande.idd%type;
    la_destination demande.destination%type;
    la_marque demande.marque%type;

    cursor c2 is
        select ido
        from typevoiture,offre
        where offre.idt = typevoiture.idt
        and typevoiture.marque = la_marque
        and offre.destination = la_destination
    ;
    cursor c3 is
        select idco
        from covoiturage
        order by idco desc
    ;
begin
    open c1;
    fetch c1 into l_idd, le_passager, la_destination, la_marque;
    if c1%notfound then
        l_idco := 1;
        l_ido := 1;
        le_passager := 1;
    else
        open c2;
        fetch c2 into l_ido;
        if c2%notfound then
            l_idco := 1;
            l_ido := 1;
            le_passager := 1;
        else
            update demande
                set status = 'passager'
                where idd = l_idd;  
            open c3;
            fetch c3 into l_idco;
            if c3%notfound then
                l_idco := 100;
            else
                l_idco := l_idco + 1;
            end if;
            close c3;
            insert into covoiturage
                values (l_idco,l_ido,le_passager);
            update demande
                set status = 'ignore'
                where idc = le_passager
                and idd != l_idd;
        end if;
        close c2;
    end if;
    close c1;
  
end;
/


insert into typevoiture values(11,'Porsche',4);
insert into offre values(21,4,11,'Berlin');

declare 
    l_idco  covoiturage.idco%type;
    l_ido  offre.ido%type;
    le_passager  client.idc%type;
begin
    traitement2(l_idco,l_ido,le_passager);
    dbms_output.put_line(l_idco || ' ' || l_ido || ' ' || le_passager);
end;
/
